import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/settings/settings_event.dart';
import 'package:flutter_hemoterapia/blocs/settings/settings_state.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_bloc.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_state.dart';
import 'package:flutter_hemoterapia/models/person.dart';
import 'package:flutter_hemoterapia/repository/person_repository.dart';
import 'package:meta/meta.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final PersonRepository repo;
  final UserDataBloc userbloc;

  SettingsBloc({ @required this.repo, this.userbloc });

  @override
  SettingsState get initialState => SettingsUninitialized();

  @override
  Stream<SettingsState> mapEventToState(
    SettingsEvent event,
  ) async* {
    if (event is SettingsFetch) {
      yield SettingsLoading();
      Person data;
      var userstate = userbloc.state; 
      data = userstate is UserDataInitialized ? userstate.getPerson() : await repo.getPersonalData();
      yield SettingsInitilized(data);
    }
    
    if (event is SettingsSave) {
      yield SettingsSaving();
      var data = await repo.savePersonalData();
      yield SettingsInitilized(data);
    }
  }
}