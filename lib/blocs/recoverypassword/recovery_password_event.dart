import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RecoveryPasswordEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RecoveryPasswordButtonPressed extends RecoveryPasswordEvent {
  final String password;

  RecoveryPasswordButtonPressed({ @required this.password });

  @override
  String toString() => 'RecoveryPasswordButtonPressed';
}

class RecoveryPasswordReset extends RecoveryPasswordEvent {
  RecoveryPasswordReset();

  @override
  String toString() => 'RecoveryPasswordReset';
}