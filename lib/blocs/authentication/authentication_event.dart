import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

/// will be dispatched when the Flutter application first loads. 
/// It will notify bloc that it needs to determine whether or not there is an existing user.
class AppStarted extends AuthenticationEvent {
  @override
  String toString() => 'AppStarted';
}

/// will be dispatched on a successful login. 
/// It will notify the bloc that the user has successfully logged in.
class LoggedIn extends AuthenticationEvent {
  final String token;

  const LoggedIn({@required this.token});

  @override
  List<Object> get props => [token];

  @override
  String toString() => 'LoggedIn { token: $token }';
}

/// will be dispatched on a successful logout. 
/// It will notify the bloc that the user has successfully logged out.
class LoggedOut extends AuthenticationEvent {
  @override
  String toString() => 'LoggedOut';
}