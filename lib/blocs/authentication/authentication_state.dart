import 'package:equatable/equatable.dart';

abstract class AuthenticationState extends Equatable {
  @override
  List<Object> get props => [];
}

/// waiting to see if the user is authenticated or not on app start.
class AuthenticationUninitialized extends AuthenticationState {
  String toString() => 'AuthenticationUninitialized';
}

/// successfully authenticated
class AuthenticationAuthenticated extends AuthenticationState {
  String toString() => 'AuthenticationAuthenticated';
}

/// successfullu authenticated first time. Allow navigate Walkthrough
class AuthenticationAuthenticatedFirstTime extends AuthenticationState {
  String toString() => 'AuthenticationAuthenticatedFirstTime';
}

/// not authenticated
class AuthenticationUnauthenticated extends AuthenticationState {
  String toString() => 'AuthenticationUnauthenticated';
}

/// waiting to persist/delete a token
class AuthenticationLoading extends AuthenticationState {
  String toString() => 'AuthenticationLoading';
}