import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/home/home_event.dart';
import 'package:flutter_hemoterapia/blocs/home/home_state.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_bloc.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_event.dart';
import 'package:flutter_hemoterapia/models/donation.dart';
import 'package:flutter_hemoterapia/models/person.dart';
import 'package:flutter_hemoterapia/repository/donation_repository.dart';
import 'package:flutter_hemoterapia/repository/person_repository.dart';
import 'package:meta/meta.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final PersonRepository personRepository;
  final DonationsRepository donationsRepository;
  final UserDataBloc userDataBloc;

  HomeBloc(
      {@required this.personRepository, @required this.donationsRepository, @required this.userDataBloc});

  @override
  HomeState get initialState => HomeUninitialized();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is HomeFetch) {
      yield* _mapProfileLoadedToState();
    }
  }

  Stream<HomeState> _mapProfileLoadedToState() async* {
    try {
      yield HomeLoading();
      Person personalData = await personRepository.getPersonalData();
      PersonalDonation personalDonation =
          await donationsRepository.getPersonDonation();
      
      userDataBloc.add(UserDataInit(personalData));
      yield HomeInitialized(
        personalDonation: personalDonation,
      );
    } catch (e) {
      yield HomeFailure(error: e.toString());
    }
  }
}
