import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_event.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_state.dart';
import 'package:flutter_hemoterapia/repository/user_repository.dart';
import 'package:meta/meta.dart';

class SignupBloc extends Bloc<SignupEvent, SignupState> {
  final UserRepository userRepository;

  SignupBloc({@required this.userRepository})
    : assert(userRepository != null);
  @override
  SignupState get initialState => SignupUnitialized();

  @override
  Stream<SignupState> mapEventToState(
    SignupEvent event,
  ) async* {
    if (event is UserRegistered) {
      yield SignupLoading();
      await userRepository.persistToken(event.token);
      yield SignupUserRegistered();
    }

    if (event is UserdataEntered) {
      yield SignupLoading();
      // TODO: Save personal data
      yield SignupUserdataEntered();
    }
  }
}