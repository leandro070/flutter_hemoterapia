
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RecoveryEmailEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RecoveryEmailReset extends RecoveryEmailEvent {
  @override
  String toString() => 'RecoveryEmailInitialize';
}

class RecoveryEmailButtonPressed extends RecoveryEmailEvent {
  final String email;

  RecoveryEmailButtonPressed({ @required this.email });

  @override
  String toString() => 'RecoveryEmailButtonPressed';
}
