import 'package:equatable/equatable.dart';
import 'package:flutter_hemoterapia/models/person.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class RegisterUserButtonPressed extends RegisterEvent {
  final String email;
  final String password;

  const RegisterUserButtonPressed({this.email, this.password});

  @override
  List<Object> get props => [email, password];
}

class UserdataButtonPressed extends RegisterEvent{
  final Person person;

  const UserdataButtonPressed({ this.person });

  @override
  List<Object> get props => [
        person
      ];
}
