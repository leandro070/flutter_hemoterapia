import 'package:bloc/bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoverycode/recovery_code_event.dart';
import 'package:flutter_hemoterapia/blocs/recoverycode/recovery_code_state.dart';

class RecoveryCodeBloc extends Bloc<RecoveryCodeEvent, RecoveryCodeState> {
  @override
  RecoveryCodeState get initialState => RecoveryCodeUninitialized();

  @override
  Stream<RecoveryCodeState> mapEventToState(
    RecoveryCodeEvent event,
  ) async* {
    if (event is RecoveryCodeButtonPressed) {
      yield* _mapRecoveryCodeButtonPressedToState(event);
    }

    if (event is RecoveryCodeReset) {
      yield* _mapRecoveryCodeResetToState(event);
    }
  }

  Stream<RecoveryCodeState> _mapRecoveryCodeButtonPressedToState(
      RecoveryCodeButtonPressed event) async* {
    try {
      yield RecoveryCodeSuccess();
    } catch (error) {
      yield RecoveryCodeFailured(error);
    }
  }

  Stream<RecoveryCodeState> _mapRecoveryCodeResetToState(
      RecoveryCodeReset event) async* {
      yield RecoveryCodeUninitialized();
  }

}