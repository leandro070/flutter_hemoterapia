class PersonalDonation {
  String nextDonation;
  int totalDonations;
  List<Donation> donations;

  PersonalDonation({this.nextDonation, this.totalDonations, this.donations});

  PersonalDonation.fromJson(Map<String, dynamic> json) {
    nextDonation = json['next_donation'];
    totalDonations = json['total_donations'];
    if (json['donations'] != null) {
      donations = new List<Donation>();
      json['donations'].forEach((v) {
        donations.add(new Donation.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['next_donation'] = this.nextDonation;
    data['total_donations'] = this.totalDonations;
    if (this.donations != null) {
      data['donations'] = this.donations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Donation {
  int id;
  String place;
  String date;
  bool capable;
  int quantity;

  Donation({this.id, this.place, this.date, this.capable, this.quantity});

  Donation.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    place = json['place'];
    date = json['date'];
    capable = json['capable'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['place'] = this.place;
    data['date'] = this.date;
    data['capable'] = this.capable;
    data['quantity'] = this.quantity;

    return data;
  }
}
