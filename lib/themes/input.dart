import 'package:flutter/material.dart';
import 'color_scheme.dart';

InputDecorationTheme outlineInputTheme() {
  return new InputDecorationTheme(
    isDense: true,
    labelStyle: TextStyle(
      color: ThemeColors.white,
      fontFamily: 'Raleway',
    ),
    hintStyle: TextStyle(
      color: ThemeColors.white60,
      fontFamily: 'Raleway',
    ),
    contentPadding: EdgeInsets.symmetric(vertical: 10),
    focusedBorder: new OutlineInputBorder(
      borderRadius: new BorderRadius.circular(35.0),
      borderSide: new BorderSide(color: ThemeColors.white),
    ),
    errorBorder: new OutlineInputBorder(
        borderRadius: new BorderRadius.circular(35.0),
        borderSide: new BorderSide(color: Colors.red)),
    focusedErrorBorder: new OutlineInputBorder(
        borderRadius: new BorderRadius.circular(35.0),
        borderSide: new BorderSide(color: Colors.red)),
    enabledBorder: new OutlineInputBorder(
      borderRadius: new BorderRadius.circular(35.0),
      borderSide: new BorderSide(color: ThemeColors.white),
    ),
  );
}

InputDecoration outlineInput({
  String placeholder = '',
  String labelText = '',
  String suffixText = '',
  IconData prefixIcon,
  IconData suffixIcon,
  Color prefixColor,
  Color suffixColor,
  Color color,
}) {
  return new InputDecoration(
    isDense: true,
    labelStyle: TextStyle(
      color: color,
      fontFamily: 'Raleway',
    ),
    hintText: placeholder,
    hintStyle: TextStyle(
      color: color,
      fontWeight: FontWeight.w400,
      fontFamily: 'Raleway',
      fontSize: 14,
    ),
    focusedBorder: new OutlineInputBorder(
      borderRadius: new BorderRadius.circular(35.0),
      borderSide: new BorderSide(color: color),
    ),
    enabledBorder: new OutlineInputBorder(
      borderRadius: new BorderRadius.circular(35.0),
      borderSide: new BorderSide(color: color),
    ),
    contentPadding: EdgeInsets.fromLTRB(0, 12, 8, 12),
    prefixIcon: prefixIcon != null ? Icon(
      prefixIcon,
      color: ThemeColors.secondary,
      size: 24,
    ) : null,
    suffixIcon: suffixIcon != null
        ? Icon(
            suffixIcon,
            color: suffixColor,
            size: 24,
          )
        : null,
    suffixStyle: TextStyle(
      color: ThemeColors.primary.withAlpha(90),
      fontFamily: "Raleway",
      fontWeight: FontWeight.w600,
    ),
    suffixText: suffixText.isNotEmpty ? suffixText : null,
  );
}

InputDecoration filledInputTheme({
  String placeholder = '',
  String labelText = '',
  IconData prefixIcon,
}) {
  return new InputDecoration(
    filled: true,
    fillColor: Colors.white,
    prefixIcon: Icon(prefixIcon),
    hintText: placeholder,
    hintStyle: TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.w600,
      fontFamily: 'Raleway',
      fontSize: 12,
    ),
    labelStyle: TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.w600,
      fontFamily: 'Raleway',
      fontSize: 12,
    ),
    contentPadding: EdgeInsets.symmetric(vertical: 16),
    focusedBorder: new UnderlineInputBorder(
      borderRadius: new BorderRadius.circular(4),
      borderSide: new BorderSide(color: Colors.transparent),
    ),
    enabledBorder: new UnderlineInputBorder(
      borderRadius: new BorderRadius.circular(4),
      borderSide: new BorderSide(color: Colors.transparent),
    ),
  );
}

InputDecoration transparentInputTheme({
  String placeholder = '',
  String labelText = '',
  IconData prefixIcon,
  IconData suffixIcon,
  Color prefixColor,
  Color suffixColor,
  Function onTapSubfix,
  Color color,
}) {
  return new InputDecoration(
    isDense: true,
    prefixIcon: prefixIcon != null ?Icon(
      prefixIcon,
      color: prefixColor,
      size: 24,
    ) : null,
    contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
    suffixIcon: suffixIcon != null ? Icon(
      suffixIcon,
      color: suffixColor,
      size: 24,
    ) : null,
    hintText: placeholder,
    hintStyle: TextStyle(
      color: color,
      fontWeight: FontWeight.w400,
      fontFamily: 'Raleway',
      fontSize: 14,
    ),
    errorBorder: new UnderlineInputBorder(
      borderSide: new BorderSide(
        color: Colors.red,
      ),
    ),
    focusedErrorBorder: new UnderlineInputBorder(
      borderSide: new BorderSide(
        color: Colors.red,
      ),
    ),
  );
}
