
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class CustomSnackbar extends StatelessWidget {
  final Widget content;
  final Widget rear;
  final Widget leading;

  CustomSnackbar({ @required this.content, this.rear, this.leading, });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(50)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            blurRadius: 20,
            offset: Offset.zero,
            color: Colors.grey.withOpacity(0.5),
          ),
        ],
      ),
      child: Row(
        children: <Widget>[
          leading == null ? Container() : leading,
          Expanded(
            child: content, // end of Container
          ),
          rear == null ? Container() : rear,
        ],
      ),
    );
  }
}
