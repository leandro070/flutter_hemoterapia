import 'dart:async';
import 'package:meta/meta.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';

class TakePictureScreen extends StatefulWidget {
  final Function(String) onTakePicture;

  TakePictureScreen({Key key, @required this.onTakePicture}) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  // Añade dos variables a la clase de estado para almacenar el CameraController
  // y el Future
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  List<CameraDescription> cameras;
  CameraDescription currentCamera;

  @override
  void initState() {
    super.initState();
    _initCameras();
  }

  @override
  void dispose() {
    // Asegúrate de eliminar el controlador cuando se elimine el Widget.
    _controller.dispose();
    super.dispose();
  }

  _initCameras() {
    availableCameras().then((cameras) {
      setState(() {
        // Obtén una cámara específica de la lista de cámaras disponibles
        currentCamera = cameras.first;
        // Para visualizar la salida actual de la cámara, es necesario crear
        // un CameraController.
        _controller = CameraController(
          currentCamera,
          // Define la resolución a utilizar
          ResolutionPreset.medium,
        );

        // A continuación, debes inicializar el controlador. Este devuelve un Future
        _initializeControllerFuture = _controller.initialize();
      });
    });
  }

  _rotateCamera() {
    if (cameras.length > 1) {
      setState(() {
        currentCamera = cameras.last;
        _controller = CameraController(
          // Obtén una cámara específica de la lista de cámaras disponibles
          currentCamera,
          // Define la resolución a utilizar
          ResolutionPreset.medium,
        );
      });
      // int index = cameras.indexOf(currentCamera);
      // if (index != -1) {

      // }
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(title: Text('Take a picture')),
      // Debes esperar hasta que el controlador se inicialice antes de mostrar la vista previa
      // de la cámara. Utiliza un FutureBuilder para mostrar un spinner de carga
      // hasta que el controlador haya terminado de inicializar.
      body: Stack(
        children: <Widget>[
          Container(
            width: size.width,
            height: size.height,
            child: FutureBuilder<void>(
              future: _initializeControllerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // Si el Future está completo, muestra la vista previa
                  return CameraPreview(_controller);
                } else {
                  // De lo contrario, muestra un indicador de carga
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ),
          new Positioned(
            bottom: 0.0,
            child: Container(
              width: size.width - 180,
              height: 60,
              margin: EdgeInsets.symmetric(horizontal: 90, vertical: 24),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(32)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FloatingActionButton(
                    child: Icon(Icons.photo),
                    heroTag: 'gallery',
                    elevation: 0,
                    onPressed: () {},
                  ),
                  ShotButton(
                    initializeControllerFuture: _initializeControllerFuture,
                    controller: _controller,
                    widget: widget,
                  ),
                  FloatingActionButton(
                    child: Icon(Icons.rotate_right),
                    heroTag: 'rotate',
                    elevation: 0,
                    onPressed: _rotateCamera,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ShotButton extends StatelessWidget {
  const ShotButton({
    Key key,
    @required Future<void> initializeControllerFuture,
    @required CameraController controller,
    @required this.widget,
  })  : _initializeControllerFuture = initializeControllerFuture,
        _controller = controller,
        super(key: key);

  final Future<void> _initializeControllerFuture;
  final CameraController _controller;
  final TakePictureScreen widget;

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.camera_alt),
      heroTag: 'shot',
      elevation: 0,
      // Agrega un callback onPressed
      onPressed: () async {
        // Toma la foto en un bloque de try / catch. Si algo sale mal,
        // atrapa el error.
        try {
          // Ensure the camera is initialized
          await _initializeControllerFuture;

          // Construye la ruta donde la imagen debe ser guardada usando
          // el paquete path.
          final path = join(
            //
            (await getTemporaryDirectory()).path,
            'hemoterapia-picture-${DateTime.now()}.png',
          );

          // Attempt to take a picture and log where it's been saved
          await _controller.takePicture(path);
          // En este ejemplo, guarda la imagen en el directorio temporal. Encuentra
          // el directorio temporal usando el plugin `path_provider`.
          widget.onTakePicture(path);
          Navigator.of(context).pop();
        } catch (e) {
          // Si se produce un error, regístralo en la consola.
          print(e);
        }
      },
    );
  }
}
