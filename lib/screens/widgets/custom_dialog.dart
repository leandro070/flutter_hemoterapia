import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class CustomDialog extends StatelessWidget {
  final Widget content;
  final String title;

  CustomDialog({@required this.content, @required this.title});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(
                this.title,
                textAlign: TextAlign.center,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Raleway',
                    fontSize: 14,
                    decoration: TextDecoration.none),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Card(
          child: Container(
            padding: EdgeInsets.fromLTRB(24, 24, 24, 8),
            width: 280,
            child: content,
          ),
          color: ThemeColors.primary,
        ),
      ],
    );
  }
}
