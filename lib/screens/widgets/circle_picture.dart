import 'dart:io';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class CirclePicture extends StatefulWidget {
  final String path;
  final bool isUrl;
  final double size;

  CirclePicture({Key key, @required this.path, this.isUrl = false, this.size })
      : assert(path != null),
        super(key: key);

  @override
  _CirclePictureState createState() => _CirclePictureState();
}

class _CirclePictureState extends State<CirclePicture> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      height: widget.size,
      width: widget.size,
      decoration: new BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey[100],
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 8,
          ),
        ],
      ),
      child: ClipOval(
        child: widget.isUrl
            ? FadeInImage.assetNetwork(
                placeholder: "assets/images/default-profile.png",
                fit: BoxFit.cover,
                image: widget.path,
              )
            : Image.file(
                File(widget.path),
                fit: BoxFit.cover,
              ),
      ),
    );
  }
}
