import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoveryemail/recovery_email.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class RecoveryPasswordUserForm extends StatefulWidget {
  @override
  _RecoveryPasswordUserFormState createState() =>
      _RecoveryPasswordUserFormState();
}

class _RecoveryPasswordUserFormState extends State<RecoveryPasswordUserForm> {
  final _formKey = GlobalKey<FormState>();

  bool _autoValidate = true;
  TextEditingController _emailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    void _onSubmitButtonPressed() {
      BlocProvider.of<RecoveryEmailBloc>(context)
          .add(RecoveryEmailButtonPressed(email: _emailController.text));
    }

    void _validateInputs() {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        _onSubmitButtonPressed();
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

    final inputEmail = Container(
      width: 250,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        controller: _emailController,
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
        decoration: InputDecoration(hintText: 'Correo electrónico'),
        validator: (value) {
          if (value.isEmpty) {
            return 'Ingrese su correo electrónico';
          }
          var regex = RegExp(
              r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
          if (!regex.hasMatch(value)) {
            return 'Correo electrónico inválido';
          }
          return null;
        },
      ),
    );

    final loading = Container(
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        backgroundColor: ThemeColors.primaryLight,
        strokeWidth: 2,
      ),
    );

    final nextText = Text(
      'Siguiente',
      style: TextStyle(
        fontFamily: 'Raleway',
        fontWeight: FontWeight.w600,
      ),
    );

    final nextBtn = Container(
      margin: EdgeInsets.only(top: 10),
      child: RaisedButton(
        textColor: Theme.of(context).primaryColor,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(35.0),
        ),
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: true ? nextText : loading,
        onPressed: _validateInputs,
      ),
    );

    final backBtn = Container(
      margin: EdgeInsets.only(
        top: 8,
        bottom: 12,
      ),
      child: RaisedButton(
        color: Colors.transparent,
        textColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(35.0),
        ),
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        child: Text(
          'Volver',
          style: TextStyle(
            fontFamily: 'Raleway',
            fontWeight: FontWeight.w600,
          ),
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );

    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Container(
        width: 250,
        margin: EdgeInsets.only(
          top: 30,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[inputEmail, nextBtn, backBtn],
        ),
      ),
    );
  }
}
