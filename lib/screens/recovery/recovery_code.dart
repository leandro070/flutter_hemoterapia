import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoverycode/recovery_code.dart';
import 'package:flutter_hemoterapia/screens/recovery/recovery_code_form.dart';
import 'package:flutter_hemoterapia/screens/recovery/widgets/header.dart';
import 'package:flutter_hemoterapia/screens/recovery/widgets/wrapper.dart';


class RecoveryCodeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RecoveryPasswordWrapper(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          HeaderLogoRecoveryPassword(
            subtitle: 'Código de recuperación',
            description:
                'Introduce el código de recuperación que recibiras en tu correo electrónico.',
          ),
          BlocProvider(
            create: (context) => RecoveryCodeBloc(),
            child: BlocConsumer<RecoveryCodeBloc, RecoveryCodeState>(
              listener: (context, state) {
                if (state is RecoveryCodeSuccess) {
                  BlocProvider.of<RecoveryCodeBloc>(context)..add(RecoveryCodeReset());
                  Navigator.pushNamed(context, '/recovery_new_password');
                }
                if (state is RecoveryCodeFailured) {
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Algo salió mal: ${state.error}'),
                    ),
                  );
                }
              },
              builder: (context, state) {
                return RecoveryCodeForm();
              },
            ),
          )
        ],
      ),
    );
  }
}
