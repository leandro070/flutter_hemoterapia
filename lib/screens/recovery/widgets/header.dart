import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HeaderLogoRecoveryPassword extends StatelessWidget {
  final String titleText = """CENTRO REGIONAL
DE HEMOTERAPIA""";
  final String description;
  final String subtitle;

  HeaderLogoRecoveryPassword(
      {@required this.description, @required this.subtitle});

  @override
  Widget build(BuildContext context) {
    final logo = Container(
      height: 50,
      child: new SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'logo',
        height: 50,
      ),
    );

    final title = Container(
      margin: EdgeInsets.only(top: 12),
      child: Text(
        titleText,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontFamily: 'Montserrat',
          fontSize: 20,
          letterSpacing: -0.5,
          fontWeight: FontWeight.w600,
          height: 1,
        ),
      ),
    );

    final subtitle = Container(
      margin: EdgeInsets.only(top: 40),
      child: Column(
        children: <Widget>[
          Text(
            this.subtitle,
            style: TextStyle(
              fontFamily: 'Raleway',
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );

    final description = Container(
      margin: EdgeInsets.only(top: 10),
      child: Text(
        this.description,
        style: TextStyle(fontFamily: 'Raleway'),
        textAlign: TextAlign.center,
      ),
    );

    return Container(
      width: 250,
      child: Column(
        children: <Widget>[logo, title, subtitle, description],
      ),
    );
  }
}
