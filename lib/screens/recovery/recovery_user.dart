import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/recoveryemail/recovery_email.dart';
import 'package:flutter_hemoterapia/screens/recovery/recovery_user_form.dart';
import 'package:flutter_hemoterapia/screens/recovery/widgets/header.dart';
import 'package:flutter_hemoterapia/screens/recovery/widgets/wrapper.dart';

class RecoveryUserScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RecoveryPasswordWrapper(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          HeaderLogoRecoveryPassword(
            subtitle: 'Recuperar contraseña',
            description:
                'Introduce tu dirección de correo electrónico de recuperación',
          ),
          BlocProvider(
            create: (context) => RecoveryEmailBloc(),
            child: BlocConsumer<RecoveryEmailBloc, RecoveryEmailState>(
              listener: (context, state) {
                if (state is RecoveryEmailSuccess) {
                  BlocProvider.of<RecoveryEmailBloc>(context).add(RecoveryEmailReset());
                  Navigator.pushNamed(context, '/recovery_code');
                }
                if (state is RecoveryEmailFailured) {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text('Algo salió mal: ${state.error}'),
                  ));
                }
              },
              builder: (context, state) {
                return RecoveryPasswordUserForm();
              },
            ),
          )
        ],
      ),
    );
  }
}
