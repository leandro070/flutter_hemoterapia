import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/home/home_bloc.dart';
import 'package:flutter_hemoterapia/blocs/home/home_event.dart';
import 'package:flutter_hemoterapia/blocs/home/home_state.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_bloc.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_state.dart';
import 'package:flutter_hemoterapia/repository/donation_repository.dart';
import 'package:flutter_hemoterapia/repository/person_repository.dart';
import 'package:flutter_hemoterapia/screens/home/widgets/background-circles.dart';
import 'package:flutter_hemoterapia/screens/home/widgets/card_donation.dart';
import 'package:flutter_hemoterapia/screens/home/widgets/card_welcome.dart';
import 'package:flutter_hemoterapia/screens/home/widgets/image_profile.dart';
import 'package:flutter_hemoterapia/screens/widgets/progress.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = '/home';
  final PersonRepository personRepository;
  final DonationsRepository donationsRepository;
  final UserDataBloc userDataBloc;
  HomeScreen({
    @required this.personRepository,
    @required this.donationsRepository,
    @required this.userDataBloc,
  });

  @override
  _HomeScreenState createState() => _HomeScreenState(
      donationsRepository: donationsRepository,
      personRepository: personRepository,
      userDataBloc: userDataBloc);
}

class _HomeScreenState extends State<HomeScreen> {
  final double sizeContainer = 300;
  final PersonRepository personRepository;
  final DonationsRepository donationsRepository;
  final UserDataBloc userDataBloc;

  _HomeScreenState({
    @required this.personRepository,
    @required this.donationsRepository,
    @required this.userDataBloc,
  });

  @override
  Widget build(BuildContext context) {
    final sizeScreen = MediaQuery.of(context).size;

    final background = Container(
      child: Column(
        children: <Widget>[
          BackgroundCircles(),
          Container(
            height: sizeScreen.height - 220 - 80,
            color: Colors.white,
          ),
        ],
      ),
    );

    final lastDonations = Container(
      margin: EdgeInsets.only(top: 20, bottom: 5, left: 40),
      width: double.infinity,
      child: Text(
        "Últimas donaciones",
        textAlign: TextAlign.left,
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w700,
          fontFamily: 'Raleway',
        ),
      ),
    );

    return BlocProvider(
      create: (context) => HomeBloc(
        donationsRepository: donationsRepository,
        personRepository: personRepository,
        userDataBloc: userDataBloc,
      )..add(HomeFetch()),

      child: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          if (state is HomeInitialized && userDataBloc.state is UserDataInitialized) {
            final userdata = (userDataBloc.state as UserDataInitialized).getPerson();
            return Container(
              child: Stack(
                children: <Widget>[
                  background,
                  Container(
                    height: sizeScreen.height,
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 38),
                          child: ProfileImage(userdata.profileUrl),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: CardWelcome(
                            totalDonations:
                                state.personalDonation.totalDonations,
                            userdata: userdata,
                            nextDonation: state.personalDonation.nextDonation,
                          ),
                        ),
                        lastDonations,
                        CardDonations(
                          donations: state.personalDonation.donations,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
          return Center(child: ProgressLogo());
        },
      ),
    );
  }

  Widget actionsButtons(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      width: sizeContainer,
      child: Column(
        children: <Widget>[
          RaisedButton(
            textColor: ThemeColors.white,
            color: ThemeColors.secondary,
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(35.0),
            ),
            elevation: 0,
            highlightElevation: 0,
            disabledElevation: 0,
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Text(
              'Hacer una donación',
              style:
                  TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w600),
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
