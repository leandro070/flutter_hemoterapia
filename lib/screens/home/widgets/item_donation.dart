import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/models/donation.dart';
import 'package:flutter_hemoterapia/screens/home/widgets/modal_donation.dart';
import 'package:flutter_hemoterapia/screens/widgets/custom_dialog.dart';
import 'package:flutter_hemoterapia/utils/custom_font_icons.dart';

class ItemDonation extends StatelessWidget {
  Donation donation;
  ItemDonation({@required this.donation});

  @override
  Widget build(BuildContext context) {
    final showDonation = () => showGeneralDialog(
          context: context,
          barrierColor: Colors.black.withOpacity(0.9),
          transitionBuilder: (context, a1, a2, widget) {
            return Transform.scale(
              scale: a1.value,
              child: Opacity(
                opacity: a1.value,
                child: CustomDialog(
                  title: "Donación",
                  content: ModalDonationDetail(
                    donation: donation,
                  ),
                ),
              ),
            );
          },
          barrierLabel: '',
          transitionDuration: Duration(milliseconds: 200),
          barrierDismissible: true,
          pageBuilder: (
            BuildContext context,
            Animation animation,
            Animation animation2,
          ) => null,
        );

    final pin = Container(
      margin: EdgeInsets.only(right: 10),
      child: Icon(
        CustomFont.pin,
        size: 14,
        color: Colors.white,
      ),
    );

    final textPlace = Text(
      donation.place,
      textAlign: TextAlign.left,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w600,
        fontFamily: 'Raleway',
      ),
    );

    final textDate = Container(
      child: Text(
        donation.date,
        textAlign: TextAlign.left,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w200,
          fontFamily: 'Raleway',
        ),
      ),
    );

    return Container(
      child: InkWell(
        onTap: () => showDonation(),
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              pin,
              Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                width: MediaQuery.of(context).size.width - 130,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[textPlace, textDate],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
