import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CircleIconLogo extends StatelessWidget {
  const CircleIconLogo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(50)),
      ),
      child: new SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'logo',
      ),
    );
  }
}
