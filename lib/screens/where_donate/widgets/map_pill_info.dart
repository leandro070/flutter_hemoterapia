import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class MapPillWrapper extends StatelessWidget {
  final Widget leading;
  final Widget content;
  final Widget rear;
  MapPillWrapper({this.leading, this.rear, @required this.content});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16, horizontal: (size.width - 300) / 2),
      height: 70,
      width: 300,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(50)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            blurRadius: 20,
            offset: Offset.zero,
            color: Colors.grey.withOpacity(0.5),
          ),
        ],
      ),
      child: Row(
        children: <Widget>[
          leading == null ? Container() : leading,
          Expanded(
            child: content, // end of Container
          ),
          rear == null ? Container() : rear,
        ],
      ),
    );
  }
}

