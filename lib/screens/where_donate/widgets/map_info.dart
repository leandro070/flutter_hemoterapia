import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/models/building.dart';

class HospitalInfo extends StatelessWidget {
  final Building buildingInfo;

  const HospitalInfo({Key key, @required this.buildingInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(buildingInfo.name,
              style: TextStyle(color: Theme.of(context).primaryColor)),
          Text(buildingInfo.hours,
              style: TextStyle(fontSize: 12, color: Colors.grey)),
          Text(buildingInfo.phone,
              style: TextStyle(fontSize: 12, color: Colors.grey)),
        ], // end of Column Widgets
      ), // end of Column
    );
  }
}