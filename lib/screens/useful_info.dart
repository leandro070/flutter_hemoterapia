import 'package:flutter/material.dart';
import 'package:flutter_hemoterapia/utils/custom_font_icons.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class UsefulInfoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      color: ThemeColors.white,
      height: size.height,
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              sectionInfo(
                context,
                "¿Qué es una donación de sangre?",
                Text(
                  "La hemodonación es un acto sencillo, rápido, prácticamente indoloro y seguro que puede salvar la vida o recuperar la salud de decenas de pacientes. La sangre es un componente que sólo puede obtenerse por vía de la donación, ya que no existe ningún producto capaz de sustituirla completamente",
                  style: TextStyle(
                    fontWeight: FontWeight.w200,
                    fontFamily: 'Raleway',
                  ),
                ),
              ),
              importantSection(
                context,
                Text.rich(
                  TextSpan(
                    text: 'Un donante',
                    style: TextStyle(
                      fontWeight: FontWeight.w200,
                      fontFamily: 'Raleway',
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: ' no es el que quiere ',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Raleway',
                        ),
                      ),
                      TextSpan(
                        text: 'sino',
                        style: TextStyle(
                          fontWeight: FontWeight.w200,
                          fontFamily: 'Raleway',
                        ),
                      ),
                      TextSpan(
                        text: ' el que puede y clasifica ',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Raleway',
                        ),
                      ),
                      TextSpan(
                        text:
                            'ya que la unidad que se extraiga es para transfundir a otro paciente. Por ello',
                        style: TextStyle(
                          fontWeight: FontWeight.w200,
                          fontFamily: 'Raleway',
                        ),
                      ),
                      TextSpan(
                        text: ' la sangre debe ser segura ',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Raleway',
                        ),
                      ),
                      TextSpan(
                        text: 'para ayudar y salvarle la vida a una persona',
                        style: TextStyle(
                          fontWeight: FontWeight.w200,
                          fontFamily: 'Raleway',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              sectionInfo(
                context,
                "Requisitos",
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Icon(
                              CustomFont.gota,
                              size: 16,
                              color: Colors.white,
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Text(
                              "Tener entre 18 y 65 años.",
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontFamily: 'Raleway',
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Icon(
                              CustomFont.gota,
                              size: 16,
                              color: Colors.white,
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Text(
                              "Pesar más de 50kg.",
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontFamily: 'Raleway',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Icon(
                              CustomFont.gota,
                              size: 16,
                              color: Colors.white,
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Text(
                              "Gozar de buen estado de salud",
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontFamily: 'Raleway',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Icon(
                              CustomFont.gota,
                              size: 16,
                              color: Colors.white,
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Text(
                              "No estar en ayunas. Desayunar infusiones o bebidas azucaradas.",
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontFamily: 'Raleway',
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Icon(
                              CustomFont.gota,
                              size: 16,
                              color: Colors.white,
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Text(
                              "Concurrir con DNI",
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontFamily: 'Raleway',
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget sectionInfo(BuildContext context, String title, Widget content) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Container(
            height: 22,
            child: Row(
              children: <Widget>[
                Container(
                  width: 3,
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  color: ThemeColors.secondary,
                ),
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    fontFamily: 'Raleway',
                  ),
                )
              ],
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8),
            child: Card(
              color: Theme.of(context).primaryColor,
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                  child: content),
            ),
          ),
        ],
      ),
    );
  }

  Widget importantSection(BuildContext context, Widget content) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
      color: Theme.of(context).primaryColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: 0.8),
              borderRadius: BorderRadius.circular(20),
            ),
            height: 37,
            width: 160,
            padding: EdgeInsets.symmetric(horizontal: 16),
            margin: EdgeInsets.only(bottom: 4),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 5),
                  child: Icon(
                    Icons.priority_high,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
                Text(
                  "Importante",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Raleway',
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8),
            child: Card(
              color: ThemeColors.secondary,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                child: content,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
