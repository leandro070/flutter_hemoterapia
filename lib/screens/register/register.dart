import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/register/register_bloc.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_bloc.dart';
import 'package:flutter_hemoterapia/blocs/signup/signup_state.dart';
import 'package:flutter_hemoterapia/repository/user_repository.dart';
import 'package:flutter_hemoterapia/screens/register/register_form.dart';
import 'package:flutter_svg/svg.dart';

class RegisterScreen extends StatelessWidget {
  final UserRepository userRepository;

  RegisterScreen({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: BlocProvider(
        create: (context) {
          return RegisterBloc(
            userRepository: userRepository,
            signupBloc: BlocProvider.of<SignupBloc>(context),
          );
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 140, bottom: 30),
            child: Center(
              child: RegisterView(),
            ),
          ),
        ),
      ),
    );
  }
}

class RegisterView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<SignupBloc, SignupState>(
      listener: (context, state) {
        if (state is SignupUserRegistered) {
          Navigator.of(context).pushNamed('/register_profile_data');
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _logoView,
          SizedBox(
            height: 30,
          ),
          RegisterForm()
        ],
      ),
    );
  }

  Widget get _logoView {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
            child: new SvgPicture.asset(
              'assets/images/logo.svg',
              semanticsLabel: 'logo',
            ),
          ),
          SizedBox(height: 20),
          Text(
            'Nuevo usuario',
            style:
                TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w600),
          ),
        ],
      ),
    );
  }
}
