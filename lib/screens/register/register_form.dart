import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/register/register_bloc.dart';
import 'package:flutter_hemoterapia/blocs/register/register_event.dart';
import 'package:flutter_hemoterapia/blocs/register/register_state.dart';

class RegisterForm extends StatefulWidget {
  RegisterForm({Key key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _passwordRepeatController = TextEditingController();
  bool _autoValidate = false;

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _onRegisterButtonPressed();
    } else {
      setState(() => _autoValidate = true);
    }
  }

  _onRegisterButtonPressed() {
    BlocProvider.of<RegisterBloc>(context).add(
      RegisterUserButtonPressed(
        email: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is RegisterFailure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text('${state.error}')),
          );
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return Container(
            child: Form(
              key: _formKey,
              autovalidate: _autoValidate,
              child: Container(
                width: 250,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 16),
                      child: TextFormField(
                        style: TextStyle(fontFamily: 'Raleway'),
                        textAlign: TextAlign.center,
                        decoration:
                            InputDecoration(hintText: 'Correo electrónico'),
                        keyboardType: TextInputType.emailAddress,
                        controller: _emailController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingrese su correo electrónico';
                          }
                          var regex = RegExp(
                              r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
                          if (!regex.hasMatch(value)) {
                            return 'Correo electrónico inválido';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 16),
                      child: TextFormField(
                        style: TextStyle(fontFamily: 'Raleway'),
                        textAlign: TextAlign.center,
                        obscureText: true,
                        decoration: InputDecoration(hintText: 'Contraseña'),
                        controller: _passwordController,
                        keyboardType: TextInputType.visiblePassword,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingrese una contraseña';
                          }
                          if (value.length < 8) {
                            return 'Debe tener al menos 8 caracteres';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 16),
                      child: TextFormField(
                        style: TextStyle(fontFamily: 'Raleway'),
                        textAlign: TextAlign.center,
                        obscureText: true,
                        decoration:
                            InputDecoration(hintText: 'Repite la contraseña'),
                        controller: _passwordRepeatController,
                        keyboardType: TextInputType.visiblePassword,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Ingrese nuevamente su contraseña';
                          }
                          if (value != _passwordController.text) {
                            return 'Las contraseñas no coinciden';
                          }
                          return null;
                        },
                      ),
                    ),
                    RaisedButton(
                      textColor: Theme.of(context).primaryColor,
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(35.0),
                      ),
                      elevation: 0,
                      highlightElevation: 0,
                      disabledElevation: 0,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: state is! RegisterLoading
                          ? Text(
                              'Siguiente',
                              style: TextStyle(
                                fontFamily: 'Raleway',
                                fontWeight: FontWeight.w600,
                              ),
                            )
                          : SizedBox(
                              height: 20,
                              width: 20,
                              child: CircularProgressIndicator(
                                backgroundColor:
                                    Theme.of(context).primaryColorLight,
                                strokeWidth: 2,
                              ),
                            ),
                      onPressed: _validateInputs,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    RaisedButton(
                      color: Colors.transparent,
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(35.0),
                      ),
                      elevation: 0,
                      highlightElevation: 0,
                      disabledElevation: 0,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Volver',
                        style: TextStyle(
                          fontFamily: 'Raleway',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
