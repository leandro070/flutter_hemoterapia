import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hemoterapia/blocs/settings/settings_bloc.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_bloc.dart';
import 'package:flutter_hemoterapia/blocs/userdata/userdata_state.dart';
import 'package:flutter_hemoterapia/repository/donation_repository.dart';
import 'package:flutter_hemoterapia/repository/person_repository.dart';
import 'package:flutter_hemoterapia/screens/home/home.dart';
import 'package:flutter_hemoterapia/screens/settings/settings.dart';
import 'package:flutter_hemoterapia/screens/useful_info.dart';
import 'package:flutter_hemoterapia/screens/where_donate/where_donate.dart';
import 'package:flutter_hemoterapia/utils/custom_font_icons.dart';
import 'package:flutter_hemoterapia/themes/color_scheme.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class DrawerScreen extends StatefulWidget {
  final PersonRepository personRepository;
  final DonationsRepository donationsRepository;

  final menuItems = [
    new DrawerItem("Mi perfil", CustomFont.user),
    new DrawerItem("Mis Donaciones", CustomFont.gota),
    new DrawerItem("Invitar a amigos", CustomFont.users),
    new DrawerItem("Donde donar", CustomFont.pin),
    new DrawerItem("Información útil", CustomFont.info),
    new DrawerItem("Configuración", CustomFont.settings),
    new DrawerItem("Cerrar sesión", CustomFont.exit)
  ];

  DrawerScreen(
      {@required this.personRepository, @required this.donationsRepository});

  @override
  DrawerScreenState createState() => DrawerScreenState();
}

class DrawerScreenState extends State<DrawerScreen> {
  int _selectedDrawerIndex = 0;
  final UserDataBloc userDataBloc = UserDataBloc();

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new HomeScreen(
          personRepository: widget.personRepository,
          donationsRepository: widget.donationsRepository,
          userDataBloc: userDataBloc,
        );
      case 3:
        return new WhereToDonateScreen();
      case 4:
        return new UsefulInfoScreen();
      case 5:
        return BlocProvider(
            create: (context) => SettingsBloc(
                repo: widget.personRepository, userbloc: userDataBloc),
            child: new SettingsScreen());
      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var drawerOptions = new Container(
      width: 250,
      child: Column(
        children: <Widget>[
          _createDrawerItem(0),
          _buildDivider(),
          _createDrawerItem(3),
          _createDrawerItem(4),
          _buildDivider(),
          _createDrawerItem(5),
          _buildDivider(),
          _createDrawerItem(
            6,
            onTap: () => Navigator.pushNamed(context, "/login"),
          ),
        ],
      ),
    );

    return BlocProvider(
      create: (context) => userDataBloc,
      child: WillPopScope(
        onWillPop: () async => false,
        child: new Scaffold(
          appBar: _buildAppLayout(),
          drawer: SizedBox(
            width: size.width,
            child: new Drawer(
              child: Container(
                color: ThemeColors.primary,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    DrawerHeader(),
                    drawerOptions,
                  ],
                ),
              ),
            ),
          ),
          body: _getDrawerItemWidget(_selectedDrawerIndex),
        ),
      ),
    );
  }

  AppBar _buildAppLayout() {
    return new AppBar(
      elevation: 4,
      centerTitle: true,
      title: new Text(
        widget.menuItems[_selectedDrawerIndex].title,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w200,
          fontFamily: 'Montserrat',
          fontSize: 18,
        ),
      ),
    );
  }

  Container _buildDivider() {
    return Container(
      height: 20,
      child: Divider(
        height: 1,
        indent: 52,
        endIndent: 10,
        color: ThemeColors.white60,
      ),
    );
  }

  InkWell _createDrawerItem(int i, {GestureTapCallback onTap}) {
    var d = widget.menuItems[i];
    return InkWell(
      onTap: onTap ?? () => _onSelectItem(i),
      child: new Container(
        height: 50,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: 50,
              child: new Icon(
                d.icon,
                color:
                    i == _selectedDrawerIndex ? Colors.white : Colors.white70,
                size: 15,
              ),
            ),
            new Flexible(
              flex: 1,
              child: Text(
                d.title,
                style: TextStyle(
                  color:
                      i == _selectedDrawerIndex ? Colors.white : Colors.white70,
                  fontSize: 16,
                ),
              ),
            ),
            new Container(
              width: 18,
              height: 2,
              margin: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                color: i == _selectedDrawerIndex
                    ? ThemeColors.secondary
                    : Colors.transparent,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DrawerHeader extends StatelessWidget {
  const DrawerHeader({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserDataBloc, UserDataState>(
      builder: (context, state) {
        if (state is UserDataInitialized) {
          return Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: 0.8),
              borderRadius: BorderRadius.circular(20),
            ),
            height: 37,
            width: 250,
            padding: EdgeInsets.symmetric(vertical: 7, horizontal: 16),
            margin: EdgeInsets.only(bottom: 32),
            child: RichText(
              text: TextSpan(
                text: "Hola " + state.getPerson().name,
                style: TextStyle(fontSize: 16, fontFamily: "Montserrat"),
                children: [
                  TextSpan(
                    text: "",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  )
                ],
              ),
            ),
          );
        }

        return Container();
      },
    );
  }
}
